import 'package:flutter/material.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 40),
        Container(
          padding: const EdgeInsets.only(left: 18),
          child: const Text(
            "Log in",
            style: TextStyle(
                fontSize: 18, fontFamily: 'Rubik', fontWeight: FontWeight.w500),
          ),
        ),
        const SizedBox(height: 8),
        Container(
          padding: const EdgeInsets.only(left: 18),
          child: const Text(
            "Sign In to continue",
            style: TextStyle(
                fontSize: 14,
                fontFamily: 'Rubik',
                color: Color.fromRGBO(106, 106, 106, 1)),
          ),
        ),
        const SizedBox(height: 28),
        Container(
          padding: const EdgeInsets.only(left: 18),
          child: const Text(
            "Username",
            style: TextStyle(fontSize: 14, fontFamily: 'Rubik'),
          ),
        ),
        const SizedBox(height: 8),
        Container(
          height: 45,
          padding: const EdgeInsets.only(left: 18, right: 18),
          child: const TextField(
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(169, 188, 207, 0.2),
                filled: true,
                label: Text(
                  "Drow19",
                  style: TextStyle(
                      fontSize: 14, fontFamily: 'Rubik', color: Colors.black),
                )),
          ),
        ),
        const SizedBox(height: 16),
        Container(
          padding: const EdgeInsets.only(left: 18),
          child: const Text(
            "Password",
            style: TextStyle(fontSize: 14, fontFamily: 'Rubik'),
          ),
        ),
        const SizedBox(height: 8),
        Container(
          height: 45,
          padding: const EdgeInsets.only(left: 18, right: 18),
          child: const TextField(
            decoration: InputDecoration(
                fillColor: Color.fromRGBO(169, 188, 207, 0.2),
                filled: true,
                label: Text("*************",
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: 'Rubik',
                        color: Colors.black))),
          ),
        ),
        const SizedBox(height: 40),
        Container(
            margin: const EdgeInsets.only(left: 18, right: 18),
            height: 45,
            alignment: Alignment.center,
            child: const Text("Log in",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    fontFamily: 'Rubik',
                    fontWeight: FontWeight.w500)),
            decoration: BoxDecoration(
                color: const Color.fromRGBO(95, 111, 140, 1),
                borderRadius: BorderRadius.circular(10))),
        const SizedBox(height: 390),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              "Dont’t have an account?",
              style: TextStyle(fontFamily: 'Rubik', fontSize: 14),
            ),
            TextButton(
                onPressed: () {},
                child: const Text(
                  "Sign up",
                  style: TextStyle(
                      fontFamily: 'Rubik',
                      fontWeight: FontWeight.w500,
                      color: Colors.black),
                ))
          ],
        ),
      ],
    ));
  }
}
