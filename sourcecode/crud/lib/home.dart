import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController nomorController = TextEditingController();
  TextEditingController namaController = TextEditingController();
  TextEditingController komoditasController = TextEditingController();
  TextEditingController luasController = TextEditingController();

  TextEditingController nomorEditController = TextEditingController();
  TextEditingController namaEditController = TextEditingController();
  TextEditingController komoditasEditController = TextEditingController();
  TextEditingController luasEditController = TextEditingController();

  List<Map<String, dynamic>> datas = [
    {
      "Nama Perusahaan": "PT. Borneo Mandiri Mineral",
      "No SK": "700/DISTAMBEN/2016",
      "Komoditas": "Emas",
      "Luas": "5.000Ha"
    },
    {
      "Nama Perusahaan": "PT. Mitra Geo Mineral",
      "No SK": "800/DISTAMBEN/2017",
      "Komoditas": "Bauksit",
      "Luas": "10.000Ha"
    },
    {
      "Nama Perusahaan": "PT. Mitra Borneo Mandiri",
      "No SK": "900/DISTAMBEN/2018",
      "Komoditas": "Zirkon",
      "Luas": "15.000Ha"
    }
  ];

  void _showDialogEdit(int index, Map<String, dynamic> data) {
    namaEditController.text = data["Nama Perusahaan"];
    nomorEditController.text = data["No SK"];
    komoditasEditController.text = data["Komoditas"];
    luasEditController.text = data["Luas"];
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text("Edit Data"),
            content: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextFormField(
                  controller: namaEditController,
                  decoration:
                      const InputDecoration(label: Text("Nama Perusahaan")),
                ),
                const SizedBox(
                  height: 3,
                ),
                TextFormField(
                  controller: nomorEditController,
                  decoration: const InputDecoration(label: Text("No SK")),
                ),
                TextFormField(
                  controller: komoditasEditController,
                  decoration: const InputDecoration(label: Text("Komoditas")),
                ),
                TextFormField(
                  controller: luasEditController,
                  decoration: const InputDecoration(label: Text("Luas")),
                )
              ],
            ),
            actions: [
              ElevatedButton(
                  onPressed: () {
                    String nama = namaEditController.text;
                    String nomor = nomorEditController.text;
                    String komoditas = komoditasEditController.text;
                    String luas = luasEditController.text;

                    datas[index]["Nama Perusahaan"] = nama;
                    datas[index]["No SK"] = nomor;
                    datas[index]["Komoditas"] = komoditas;
                    datas[index]["Luas"] = luas;

                    namaEditController.text = "";
                    nomorEditController.text = "";
                    komoditasEditController.text = "";
                    luasEditController.text = "";

                    setState(() {});
                    Navigator.of(context).pop();
                  },
                  child: const Text("Edit")),
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text("Cancel"))
            ],
          );
        });
  }

  void _showDialogAdd() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text("Add Data"),
            content: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextFormField(
                  controller: namaController,
                  decoration:
                      const InputDecoration(label: Text("Nama Perusahaan")),
                ),
                const SizedBox(
                  height: 3,
                ),
                TextFormField(
                  controller: nomorController,
                  decoration: const InputDecoration(label: Text("No SK")),
                ),
                TextFormField(
                  controller: komoditasController,
                  decoration: const InputDecoration(label: Text("Komoditas")),
                ),
                TextFormField(
                  controller: luasController,
                  decoration: const InputDecoration(label: Text("Luas")),
                )
              ],
            ),
            actions: [
              ElevatedButton(
                  onPressed: () {
                    String nama = namaController.text;
                    String nomor = nomorController.text;
                    String komoditas = komoditasController.text;
                    String luas = luasController.text;

                    datas.add({
                      "Nama Perusahaan": nama,
                      "No SK": nomor,
                      "Komoditas": komoditas,
                      "Luas": luas
                    });

                    namaEditController.text = "";
                    nomorEditController.text = "";
                    komoditasEditController.text = "";
                    luasEditController.text = "";

                    setState(() {});
                    Navigator.of(context).pop();
                  },
                  child: const Text("Add")),
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text("Cancel"))
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daftar Izin Usaha Pertambangan"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _showDialogAdd,
        child: const Icon(Icons.add),
      ),
      body: Container(
        padding: const EdgeInsets.all(5),
        child: ListView.builder(
            itemCount: datas.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                trailing: SizedBox(
                  width: 60,
                  child: Row(
                    children: [
                      InkWell(
                        onTap: () {
                          setState(() {
                            datas.removeAt(index);
                          });
                        },
                        child: const Icon(Icons.delete),
                      ),
                      const SizedBox(
                        width: 3,
                      ),
                      InkWell(
                        onTap: () {
                          _showDialogEdit(index, datas[index]);
                        },
                        child: const Icon(Icons.edit),
                      ),
                    ],
                  ),
                ),
                title: Text(
                  datas[index]["Nama Perusahaan"],
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(datas[index]["No SK"]),
                    Text(datas[index]["Komoditas"]),
                    Text(datas[index]["Luas"])
                  ],
                ),
                // subtitle: Text(
                //   (datas[index]["No SK"]),
                // ),
                // isThreeLine: true,
              );
            }),
      ),
    );
  }
}
